grammar Gram;

/* -------------- parser rules -------------- */

program
  : block '.'
  ;

block
  : blockConst?  blockVar?  blockProc* blockCommand
  ;

blockConst
  : CONST_KEYWORD symbolDeclAssign (',' symbolDeclAssign)* END_STATEMENT
  ;

blockVar
  : VARIABLE_KEYWORD symbol (',' symbol)* END_STATEMENT
  ;

blockProc
  : PROCEDURE_KEYWORD IDENT '(' (procParameter (',' procParameter)*)? ')' '{' block '}'
  ;

command
  : 'if' '(' expression ')' '{' blockCommand '}' ('else' '{' blockCommand '}')?     #ifCommand
  | 'for' forControl '{' blockCommand '}'                                           #forCommand
  | 'while' '(' expression ')' '{' blockCommand '}'                                 #whileCommand
  | 'do' '{' blockCommand '}' 'while' '(' expression ')'                            #doWhileCommand
  | 'repeat' '{' blockCommand '}' 'until' '(' expression ')'                        #repeatUntilCommand
  | 'switch' '(' expression ')' '{' switchCases+ switchDefaultCase?'}'              #switchCommand
  | procedureCall END_STATEMENT                                                     #procedureCallCommand
  | variableAssigment END_STATEMENT                                                 #variableAssigmentCommand
  ;

 blockCommand
  : command*
  ;

expression
  : possibleValues                                          #valueExpression
  | IDENT                                                   #identExpression
  | expression MULT_DIV expression                          #multDivExpression
  | expression PLUS_MINUS expression                        #plusMinusExpression
  | expression LOG_COMPARISONS expression                   #compExpression
  | expression AND_OR expression                            #andOrExpression
  | '(' expression ')'                                      #parenExpression
  | NEGATION expression                                     #negationExpression
  | PLUS_MINUS expression                                   #plusMinusBeforeExpression
  | IDENT ADDRESS_KEYWORD                                   #identAddressExpression
  ;

procedureCall
  : IDENT '(' (expression (',' expression)*)? ')'
  ;

possibleTypes
  : INT
  | BOOLEAN
  ;

possibleValues
  : (PLUS_MINUS? INT_LITERAL)
  | BOOLEAN_LITERAL
  ;

variableAssigment
  : IDENT ASSIGN expression
  ;

forControl
  : '(' variableAssigment END_STATEMENT expression END_STATEMENT variableAssigment ')'
  ;

switchCases
  : 'case' possibleValues ':' '{' blockCommand '}'
  ;

switchDefaultCase
  : 'default' ':' '{' blockCommand '}'
  ;

symbol
  : possibleTypes IDENT
  ;

symbolDeclAssign
  : symbol ASSIGN expression
  ;

procParameter
  : symbol ADDRESS_KEYWORD?;


/* -------------- lexical rules -------------- */

/* ------ Key words ------ */
CONST_KEYWORD : 'const' ;
VARIABLE_KEYWORD : 'var' ;
PROCEDURE_KEYWORD : 'proc' ;
ADDRESS_KEYWORD : '#' ;

/* ------ Types ------ */
BOOLEAN : 'boolean' ;
INT   : 'int' ;
STRING : 'string';

BOOLEAN_LITERAL: 'true' | 'false' ;

/* ------ Operators ------ */
ASSIGN : '=';
END_STATEMENT: ';';
PLUS_MINUS: '+' |'-';
MULT_DIV: '*' |'/';
AND_OR:  '&&' |'||';
NEGATION : '!';
LOG_COMPARISONS: '>' | '>=' | '<' | '<=' | '==' | '!=';


/* ------ Literals ------ */

IDENT : ([a-zA-Z])([a-zA-Z0-9])*;

INT_LITERAL : [0-9]+;

WHITESPACE : [ \r\t\n]+ -> skip ;

LINE_COMMENT : '//' ~[\r\n]* -> skip;