package visitors;

import gen.GramBaseVisitor;
import gen.GramParser;
import types.Block;
import types.Symbol;
import types.enums.SymbolType;
import types.procedure.Procedure;

import java.util.ArrayList;

public class ProcedureVisitor extends GramBaseVisitor<Procedure> {

    @Override
    public Procedure visitBlockProc(GramParser.BlockProcContext ctx) {
        
        BlockVisitor blockVisitor = new BlockVisitor();
        Block childBlock = blockVisitor.visitBlock(ctx.block());
        
        String name = ctx.IDENT().toString();
        
        SymbolVisitor symbolVisitor = new SymbolVisitor();
        ArrayList<Symbol> parameters = new ArrayList<>();
        for (GramParser.ProcParameterContext procParameterContext : ctx.procParameter()) {
            Symbol symbol =  symbolVisitor.visitSymbol(procParameterContext.symbol());

            // parameter with * meaning address to variable is handled
            if(procParameterContext.ADDRESS_KEYWORD()!=null){
                if(symbol.getDataType() == SymbolType.BOOLEAN){
                    symbol.setType(SymbolType.BOOLEAN_ADDR);
                }else if(symbol.getDataType() == SymbolType.INT){
                    symbol.setType(SymbolType.INT_ADDR);
                }
            }

            symbol.setConstant(false);
            parameters.add(symbol);
        }



        return new Procedure(childBlock,name,parameters);
    }
}
