package visitors;

import gen.GramBaseVisitor;
import gen.GramParser;
import types.command.BlockCommand;
import types.command.Command;

import java.util.ArrayList;
import java.util.List;

public class BlockCommandVisitor extends GramBaseVisitor<BlockCommand> {

    @Override
    public BlockCommand visitBlockCommand(GramParser.BlockCommandContext ctx) {
        List<Command> commands = new ArrayList<>();
        if(ctx != null){
            CommandVisitor commandVisitor = new CommandVisitor();
            for (GramParser.CommandContext commandContext : ctx.command()) {
                Command command = commandVisitor.visit(commandContext);
                commands.add(command);
            }
        }
        return new BlockCommand(commands);
    }
}
