package visitors;

import gen.GramBaseVisitor;
import gen.GramParser;
import types.Block;
import types.Symbol;
import types.command.BlockCommand;
import types.procedure.Procedure;

import java.util.ArrayList;
import java.util.List;

public class BlockVisitor extends GramBaseVisitor<Block>{

    @Override
    public Block visitBlock(GramParser.BlockContext ctx) {

        // visit block definitions
        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        BlockCommand commands = blockCommandVisitor.visitBlockCommand(ctx.blockCommand());
        List<Procedure> procedures=  new ArrayList<>();
        List<Symbol> symbols = new ArrayList<>();

        // visit procedure definitions
        ProcedureVisitor procedureVisitor = new ProcedureVisitor();
        if(ctx.blockProc() != null){
            for (GramParser.BlockProcContext blockProcContext : ctx.blockProc()) {
                Procedure procedure = procedureVisitor.visitBlockProc(blockProcContext);
                procedures.add(procedure);
            }
        }

        // visit constants definitions
        SymbolVisitor symbolVisitor = new SymbolVisitor();
        if(ctx.blockConst() != null){
            for (GramParser.SymbolDeclAssignContext symbolDeclAssignCtx : ctx.blockConst().symbolDeclAssign()) {
                Symbol symbol = symbolVisitor.visitSymbolDeclAssign(symbolDeclAssignCtx);
                symbol.setConstant(true);
                symbols.add(symbol);
            }
        }

        // visit variables definitions
        if(ctx.blockVar() != null){
            for (GramParser.SymbolContext symbolCtx : ctx.blockVar().symbol()) {
                Symbol symbol = symbolVisitor.visitSymbol(symbolCtx);
                symbol.setConstant(false);
                symbols.add(symbol);
            }
        }

        return new Block(commands,procedures,symbols);
    }



}
