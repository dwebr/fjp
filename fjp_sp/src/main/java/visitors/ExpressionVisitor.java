package visitors;

import types.Value;
import types.command.Command;
import types.enums.ExpressionType;
import types.expression.BinaryOperExpression;
import types.expression.Expression;
import gen.GramBaseVisitor;
import gen.GramParser;
import types.expression.UnaryOperExpression;
import types.expression.ValueExpression;


public class ExpressionVisitor extends GramBaseVisitor<Expression> {

    @Override
    public Expression visitNegationExpression(GramParser.NegationExpressionContext ctx) {
        Expression childExpression = this.visit(ctx.expression());
        return new UnaryOperExpression(ExpressionType.NEGATION,childExpression);
    }

    @Override
    public Expression visitValueExpression(GramParser.ValueExpressionContext ctx) {
        ValueVisitor valueVisitor = new ValueVisitor();
        Value value = valueVisitor.visitPossibleValues(ctx.possibleValues());
        return new ValueExpression(ExpressionType.VALUE,value);
    }

    @Override
    public Expression visitPlusMinusExpression(GramParser.PlusMinusExpressionContext ctx) {
        Expression childExpression0 = this.visit(ctx.expression(0));
        Expression childExpression1 = this.visit(ctx.expression(1));
        ExpressionType type = null;
        switch(ctx.PLUS_MINUS().getText()){
            case "+":type = ExpressionType.PLUS; break;
            case "-":type = ExpressionType.MINUS; break;
        }

        return new BinaryOperExpression(type,childExpression0,childExpression1);
    }

    @Override
    public Expression visitPlusMinusBeforeExpression(GramParser.PlusMinusBeforeExpressionContext ctx) {
        Expression childExpression = this.visit(ctx.expression());

        ExpressionType type = null;
        switch(ctx.PLUS_MINUS().getText()){
            case "+": type = ExpressionType.PLUS_BEFORE; break;
            case "-":type = ExpressionType.MINUS_BEFORE; break;
        }

        return new UnaryOperExpression(type,childExpression);
    }

    @Override
    public Expression visitCompExpression(GramParser.CompExpressionContext ctx) {
        Expression childExpression0 = this.visit(ctx.expression(0));
        Expression childExpression1 = this.visit(ctx.expression(1));
        ExpressionType type = null;
        switch(ctx.LOG_COMPARISONS().getText()){
            case ">": type = ExpressionType.GT; break;
            case ">=":type = ExpressionType.GE; break;
            case "<": type = ExpressionType.LT; break;
            case "<=": type = ExpressionType.LE; break;
            case "==": type = ExpressionType.EQ; break;
            case "!=": type = ExpressionType.NOT_EQ; break;
        }

        return new BinaryOperExpression(type,childExpression0,childExpression1);
    }

    @Override
    public Expression visitAndOrExpression(GramParser.AndOrExpressionContext ctx) {
        Expression childExpression0 = this.visit(ctx.expression(0));
        Expression childExpression1 = this.visit(ctx.expression(1));
        ExpressionType type = null;
        switch(ctx.AND_OR().getText()){
            case "&&": type = ExpressionType.AND; break;
            case "||":type = ExpressionType.OR; break;
        }

        return new BinaryOperExpression(type,childExpression0,childExpression1);
    }

    @Override
    public Expression visitMultDivExpression(GramParser.MultDivExpressionContext ctx) {
        Expression childExpression0 = this.visit(ctx.expression(0));
        Expression childExpression1 = this.visit(ctx.expression(1));
        ExpressionType type = null;
        switch(ctx.MULT_DIV().getText()){
            case "*":type = ExpressionType.MULT; break;
            case "/":type = ExpressionType.DIV; break;
        }

        return new BinaryOperExpression(type,childExpression0,childExpression1);
    }

    @Override
    public Expression visitParenExpression(GramParser.ParenExpressionContext ctx) {
        Expression childExpression = this.visit(ctx.expression());
        return new UnaryOperExpression(ExpressionType.PAREN,childExpression);
    }

    @Override
    public Expression visitIdentExpression(GramParser.IdentExpressionContext ctx) {
        Value value = new Value(ctx.IDENT().getText());
        return new ValueExpression(ExpressionType.IDENT,value);
    }

    @Override
    public Expression visitIdentAddressExpression(GramParser.IdentAddressExpressionContext ctx) {
        Value value = new Value(ctx.IDENT().getText());
        return new ValueExpression(ExpressionType.IDENT_ADDRESS,value);
    }
}
