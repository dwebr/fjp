package visitors;

import gen.GramBaseVisitor;
import gen.GramParser;
import types.Value;
import types.enums.SymbolType;

public class ValueVisitor extends GramBaseVisitor<Value> {

    @Override
    public Value visitPossibleValues(GramParser.PossibleValuesContext ctx) {
        Value value = new Value(ctx.getText());
        if(ctx.BOOLEAN_LITERAL() != null){
            value.setType(SymbolType.BOOLEAN);
        }else if (ctx.INT_LITERAL() != null) {
            value.setType(SymbolType.INT);
        }
        return value;
    }
}
