package visitors;

import gen.GramBaseVisitor;
import gen.GramParser;
import types.Value;
import types.command.*;
import types.enums.CommandType;
import types.enums.SymbolType;
import types.expression.Expression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommandVisitor extends GramBaseVisitor<Command> {
    @Override
    public Command visitIfCommand(GramParser.IfCommandContext ctx) {
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();

        BlockCommand commandsBlockIfTrue = blockCommandVisitor.visitBlockCommand(ctx.blockCommand(0));
        BlockCommand commandsBlockIfFalse = blockCommandVisitor.visitBlockCommand(ctx.blockCommand(1));

        return new IfCommand(expression,commandsBlockIfTrue,commandsBlockIfFalse, CommandType.IF);
    }

    @Override
    public Command visitForCommand(GramParser.ForCommandContext ctx) {

        VarAssigmentCommand initialAssigment = getVarAssigment(ctx.forControl().variableAssigment(0));
        VarAssigmentCommand step = getVarAssigment(ctx.forControl().variableAssigment(1));

        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.forControl().expression());

        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        BlockCommand commandsBlock = blockCommandVisitor.visitBlockCommand(ctx.blockCommand());

        return new ForCommand(initialAssigment,expression,step,commandsBlock, CommandType.FOR);
    }

    @Override
    public Command visitWhileCommand(GramParser.WhileCommandContext ctx) {
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        BlockCommand commandsBlock = blockCommandVisitor.visitBlockCommand(ctx.blockCommand());

        return new WhileCommand(expression,commandsBlock, CommandType.WHILE);
    }

    @Override
    public Command visitDoWhileCommand(GramParser.DoWhileCommandContext ctx) {
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        BlockCommand commandsBlock = blockCommandVisitor.visitBlockCommand(ctx.blockCommand());

        return new WhileCommand(expression,commandsBlock, CommandType.DO_WHILE);
    }

    @Override
    public Command visitRepeatUntilCommand(GramParser.RepeatUntilCommandContext ctx) {
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        BlockCommand commandsBlock = blockCommandVisitor.visitBlockCommand(ctx.blockCommand());

        return new WhileCommand(expression,commandsBlock, CommandType.REPEAT_UNTIL);
    }

    @Override
    public Command visitSwitchCommand(GramParser.SwitchCommandContext ctx) {
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        HashMap<Value, BlockCommand> cases = new HashMap<>();
        ValueVisitor valueVisitor = new ValueVisitor();
        BlockCommandVisitor blockCommandVisitor = new BlockCommandVisitor();
        for (GramParser.SwitchCasesContext switchCase : ctx.switchCases()) {
            Value value = valueVisitor.visitPossibleValues(switchCase.possibleValues());
            BlockCommand blockCommand = blockCommandVisitor.visitBlockCommand(switchCase.blockCommand());
            cases.put(value,blockCommand);
        }

        BlockCommand defaultCase = null;
        if(ctx.switchDefaultCase() != null){
            defaultCase = blockCommandVisitor.visitBlockCommand(ctx.switchDefaultCase().blockCommand());
        }

        return new SwitchCommand(expression,cases,defaultCase,CommandType.SWITCH);
    }


    @Override
    public Command visitProcedureCallCommand(GramParser.ProcedureCallCommandContext ctx) {

        String ident = ctx.procedureCall().IDENT().getText();

        List<Expression> parameters = new ArrayList<>();
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        for (GramParser.ExpressionContext expressionContext : ctx.procedureCall().expression()) {
            Expression expression = expressionVisitor.visit(expressionContext);
            parameters.add(expression);
        }

        return new ProcedureCallCommand(ident,parameters, CommandType.PROCEDURE_CALL);
    }

    @Override
    public Command visitVariableAssigmentCommand(GramParser.VariableAssigmentCommandContext ctx) {
        return getVarAssigment(ctx.variableAssigment());
    }

    private VarAssigmentCommand getVarAssigment(GramParser.VariableAssigmentContext ctx){

        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());

        String ident = ctx.IDENT().getText();

        return new VarAssigmentCommand(expression,ident, CommandType.ASSIGMENT);
    }

}
