package visitors;

import types.expression.Expression;
import gen.GramBaseVisitor;
import gen.GramParser;
import types.Symbol;
import types.enums.SymbolType;

public class SymbolVisitor extends GramBaseVisitor<Symbol> {

    @Override
    public Symbol visitSymbol(GramParser.SymbolContext ctx) {
        String name = ctx.IDENT().toString();
        SymbolType symbolType = SymbolType.valueOf(ctx.possibleTypes().getText().toUpperCase());
        return new Symbol(name,symbolType);
    }

    @Override
    public Symbol visitSymbolDeclAssign(GramParser.SymbolDeclAssignContext ctx) {
        String name = ctx.symbol().IDENT().toString();
        SymbolType symbolType = SymbolType.valueOf(ctx.symbol().possibleTypes().getText().toUpperCase());
        ExpressionVisitor expressionVisitor = new ExpressionVisitor();
        Expression expression = expressionVisitor.visit(ctx.expression());
        return new Symbol(name,symbolType,expression);
    }
}
