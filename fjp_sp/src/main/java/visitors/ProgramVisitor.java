package visitors;

import org.jetbrains.annotations.NotNull;
import types.Program;
import gen.GramBaseVisitor;
import gen.GramParser;

public class ProgramVisitor extends GramBaseVisitor<Program>{
    @Override
    public Program visitProgram(GramParser.ProgramContext ctx) {
        BlockVisitor blockVisitor = new BlockVisitor();
        return new Program(blockVisitor.visitBlock(ctx.block()));
    }
}
