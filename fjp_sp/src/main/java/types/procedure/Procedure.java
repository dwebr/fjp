package types.procedure;

import types.Block;
import types.Symbol;

import java.util.List;

public class Procedure {
    private Block block;
    private final String name;
    private final List<Symbol> parameters;

    public Procedure(Block block, String name, List<Symbol> parameters) {
        this.block = block;
        this.name = name;
        this.parameters = parameters;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public String getName() {
        return name;
    }

    public List<Symbol> getParameters() {
        return parameters;
    }

}
