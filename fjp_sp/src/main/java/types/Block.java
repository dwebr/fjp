package types;

import types.command.BlockCommand;
import types.procedure.Procedure;

import java.util.List;

/**
 * Represents block as procedure body or whole program word
 */
public class Block {
    /**
     * commands used in block
     */
    private final BlockCommand commands;
    /**
     * procedure definitions
     */
    private final List<Procedure> procedures;
    /**
     * constant and values definitions
     */
    private final List<Symbol> symbols;

    public Block(BlockCommand commands, List<Procedure> procedures, List<Symbol> symbols) {
        this.commands = commands;
        this.procedures = procedures;
        this.symbols = symbols;
    }

    public BlockCommand getCommands() {
        return commands;
    }

    public List<Procedure> getProcedures() {
        return procedures;
    }

    public List<Symbol> getSymbols() {
        return symbols;
    }
}