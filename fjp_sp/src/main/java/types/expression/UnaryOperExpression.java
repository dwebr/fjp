package types.expression;

import types.enums.ExpressionType;

/**
 * expression using one operand and one operator
 */
public class UnaryOperExpression extends Expression {

    Expression expression;

    public UnaryOperExpression(ExpressionType type, Expression expression) {
        super(type);
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }
}
