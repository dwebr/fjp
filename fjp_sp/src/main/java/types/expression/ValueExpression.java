package types.expression;

import types.Value;
import types.enums.ExpressionType;

/**
 * Expression described by one value
 */
public class ValueExpression  extends Expression{

    Value value;

    public ValueExpression(ExpressionType type, Value value) {
        super(type);
        this.value = value;
    }

    public Value getValue() {
        return value;
    }
}
