package types.expression;

import types.enums.ExpressionType;
import types.enums.SymbolType;

public class Expression {
    private final ExpressionType type;
    private SymbolType resultType;

    public Expression(ExpressionType type) {
        this.type = type;
    }


    public void setResultType(SymbolType resultType) {
        this.resultType = resultType;
    }

    public SymbolType getResultType() {
        return resultType;
    }

    public ExpressionType getType() {
        return type;
    }


}
