package types.expression;

import types.enums.ExpressionType;

/**
 * expression using two operands and one operator
 */
public class BinaryOperExpression extends Expression{

    Expression expression1;
    Expression expression2;

    public BinaryOperExpression(ExpressionType type, Expression expression1, Expression expression2) {
        super(type);
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    public Expression getExpression1() {
        return expression1;
    }

    public Expression getExpression2() {
        return expression2;
    }
}
