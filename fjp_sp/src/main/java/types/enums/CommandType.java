package types.enums;

public enum CommandType {
    WHILE,
    DO_WHILE,
    FOR,
    REPEAT_UNTIL,
    IF,
    SWITCH,
    ASSIGMENT,
    PROCEDURE_CALL,
}
