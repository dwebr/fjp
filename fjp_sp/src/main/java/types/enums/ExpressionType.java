package types.enums;

public enum ExpressionType {
    VALUE ("VALUE"),
    IDENT ("IDENTIFICATION"),
    IDENT_ADDRESS ("IDENTIFICATION ADDRESS"), // int a#
    MULT ("*"),
    DIV ("/"),
    PLUS ("+"),
    MINUS ("-"),
    EQ ("=="),
    NOT_EQ ("!="),
    LT("<"),
    LE("<="),
    GE(">="),
    GT(">"),
    AND("&&"),
    OR("||"),
    PAREN("()"),
    NEGATION("!"),
    PLUS_BEFORE("+ BEFORE EXPRESSION"),
    MINUS_BEFORE("- BEFORE EXPRESSION");

    private final String name;

    ExpressionType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
