package types.enums;

public enum SymbolType {
    INT("INT"),
    BOOLEAN("BOOLEAN"),
    INT_ADDR("INT ADDRESS"),
    BOOLEAN_ADDR("BOOLEAN ADDRESS");

    private final String name;

    SymbolType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
