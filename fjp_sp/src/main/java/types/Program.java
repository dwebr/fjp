package types;

/**
 * Represent of program in internal structure
 */
public class Program {

    private final Block block;

    public Program(Block block)
    {
        this.block = block;
    }

    public Block getBlock()
    {
        return block;
    }
}