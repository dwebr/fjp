package types;

import types.expression.Expression;
import types.enums.SymbolType;


/**
 * Class representing value, constant or procedure parameter
 */
public class Symbol {
    private final String name;
    private Expression expression = null;
    private boolean isConstant;
    private SymbolType type;

    public Symbol(String name, SymbolType dataType, Expression expression) {
        this.name = name;
        this.expression = expression;
        this.type = dataType;
    }

    public Symbol(String name, SymbolType dataType) {
        this.name = name;
        this.type = dataType;
    }

    public void setConstant(boolean constant) {
        isConstant = constant;
    }

    public String getName() {
        return name;
    }

    public Expression getExpression() {
        return expression;
    }

    public boolean isConstant() {
        return isConstant;
    }

    public SymbolType getDataType() {
        return type;
    }

    public void setType(SymbolType type) {
        this.type = type;
    }

    public boolean isAddress(){
        return type == SymbolType.INT_ADDR || type == SymbolType.BOOLEAN_ADDR;
    }

    /**
     * return symbol size on stack
     * @return
     */
    public int getSize(){
        if(isAddress()){
            return 2; // address has on stack 2 values -> level and address
        }else{
            return 1;
        }
    }
}
