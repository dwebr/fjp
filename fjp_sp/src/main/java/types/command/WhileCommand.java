package types.command;

import types.enums.CommandType;
import types.expression.Expression;

import java.util.List;

public class WhileCommand extends Command{
    private final Expression expression;
    private final BlockCommand body;

    public WhileCommand(Expression expression, BlockCommand body, CommandType type) {
        super(type);
        this.expression = expression;
        this.body = body;
    }

    public Expression getExpression() {
        return expression;
    }

    public BlockCommand getBody() {
        return body;
    }
}
