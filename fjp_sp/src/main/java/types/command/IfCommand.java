package types.command;

import types.enums.CommandType;
import types.expression.Expression;

import java.util.List;

public class IfCommand extends Command{
    private final Expression expression;
    private final BlockCommand commandsIfTrue;
    private final BlockCommand commandsIfFalse;

    public IfCommand(Expression expression, BlockCommand commandsIfTrue, BlockCommand commandsIfFalse, CommandType type) {
        super(type);
        this.expression = expression;
        this.commandsIfTrue = commandsIfTrue;
        this.commandsIfFalse = commandsIfFalse;
    }

    public Expression getExpression() {
        return expression;
    }

    public BlockCommand getCommandsIfTrue() {
        return commandsIfTrue;
    }

    public BlockCommand getCommandsIfFalse() {
        return commandsIfFalse;
    }
}
