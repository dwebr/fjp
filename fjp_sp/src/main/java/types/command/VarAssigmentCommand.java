package types.command;

import types.enums.CommandType;
import types.expression.Expression;


public class VarAssigmentCommand extends Command{
    private final Expression expression;
    private final String name;

    public VarAssigmentCommand(Expression expression, String name, CommandType type) {
        super(type);
        this.expression = expression;
        this.name = name;
    }

    public Expression getExpression() {
        return expression;
    }

    public String getName() {
        return name;
    }

}
