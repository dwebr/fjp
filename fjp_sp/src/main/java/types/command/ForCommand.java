package types.command;

import types.enums.CommandType;
import types.expression.Expression;

import java.util.List;

public class ForCommand extends Command{
    private final VarAssigmentCommand initialAssigment;
    private final Expression expression;
    private final VarAssigmentCommand step;

    private final BlockCommand body;

    public ForCommand(VarAssigmentCommand initialAssigment, Expression expression, VarAssigmentCommand step, BlockCommand body, CommandType type) {
        super(type);
        this.initialAssigment = initialAssigment;
        this.expression = expression;
        this.step = step;
        this.body = body;
    }

    public VarAssigmentCommand getInitialAssigment() {
        return initialAssigment;
    }

    public Expression getExpression() {
        return expression;
    }

    public VarAssigmentCommand getStep() {
        return step;
    }

    public BlockCommand getBody() {
        return body;
    }
}
