package types.command;

import java.util.List;

public class BlockCommand {
    private final List<Command> commands;

    public BlockCommand(List<Command> commands) {
        this.commands = commands;
    }

    public List<Command> getCommands() {
        return commands;
    }
}
