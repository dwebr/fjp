package types.command;

import types.Value;
import types.enums.CommandType;
import types.expression.Expression;

import java.util.ArrayList;
import java.util.HashMap;

public class SwitchCommand extends Command{
    private final Expression expression;
    private final HashMap<Value, BlockCommand> cases;
    private final BlockCommand defaultCase;


    public SwitchCommand(Expression expression, HashMap<Value, BlockCommand> cases, BlockCommand defaultCase, CommandType type) {
        super(type);
        this.expression = expression;
        this.cases = cases;
        this.defaultCase = defaultCase;
    }

    public Expression getExpression() {
        return expression;
    }

    public BlockCommand getDefaultCase() {
        return defaultCase;
    }

    public HashMap<Value, BlockCommand> getCases() {
        return cases;
    }
}
