package types.command;

import types.enums.CommandType;
import types.expression.Expression;

import java.util.List;

public class ProcedureCallCommand extends Command{
    private final String name;
    private final List<Expression> parameters;

    public ProcedureCallCommand(String name, List<Expression> parameters, CommandType type) {
        super(type);
        this.name = name;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public List<Expression> getParameters() {
        return parameters;
    }
}
