package types;

import types.enums.SymbolType;
import visitors.SymbolVisitor;

/**
 * represents code literal or identification name used in expression
 */
public class Value
{
    private final Object value;
    private SymbolType type;

    public Value(Object value)
    {
        this.value = value;
    }

    public void setType(SymbolType type) {
        this.type = type;
    }

    public int toInt()
    {
        switch(type){
            case INT:
                return Integer.parseInt(this.value.toString());
            case BOOLEAN:
                return Boolean.parseBoolean(this.value.toString()) ? 1 : 0;
        }
        return 0;
    }

    @Override
    public String toString()
    {
        return (String) this.value;
    }

    public SymbolType getType() {
        return type;
    }
}