package instruction;

public enum InstructionOperationType {
    UNARY_MINUS(1),
    PLUS(2),
    MINUS(3),
    MULT(4),
    DIV(5),
    MOD(6),
    ODD(7),
    EQ(8),
    NOT_EQ(9),
    LT(10),
    GE(11),
    GT(12),
    LE(13);

    private final int value;

    InstructionOperationType(int value) {
        this.value = value;
    }

    public int toInt() {
        return this.value;
    }
}
