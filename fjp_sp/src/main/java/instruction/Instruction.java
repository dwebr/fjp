package instruction;


/**
 * class representing instruction of PL/0 extended instruction set
 */
public class Instruction {
    int level;
    int address;
    InstructionType type;

    public Instruction(int level, int address, InstructionType type) {
        this.level = level;
        this.address = address;
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "\t" + type.toString() + "\t" + level + "\t" + address + "\n";
    }

    public void setAddress(int address) {
        this.address = address;
    }
}
