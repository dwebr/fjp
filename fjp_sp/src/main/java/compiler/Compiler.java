package compiler;

import instruction.Instruction;
import symbolTable.SymbolTable;
import types.Program;
import instruction.InstructionType;

import java.util.ArrayList;

public class Compiler {

    public static int BLOCK_DEFAULT_STACK_SIZE = 3;

    private final Program program;

    /**
     * instructions list
     */
    private static final ArrayList<Instruction> instructions = new ArrayList<>();

    /**
     * table of symbols
     */
    private static final SymbolTable symbolTable = new SymbolTable();


    public Compiler(Program program)
    {
        this.program = program;
    }

    public void compile()
    {
        new BlockCompiler(program.getBlock()).compile();
    }

    /**
     * add new instructions
     * @return index of the added instruction
     */
    public static int putInstruction(InstructionType instructionType, int level, int address)
    {
        instructions.add(new Instruction( level, address,instructionType ));
        return instructions.size() - 1;
    }

    /**
     * @return index of the next instruction
     */
    public static int getNextInstructionIndex(){
        return instructions.size();
    }

    /**
     * set instruction address to point behind the last instruction
     * @param instructionAddress instruction that will be edited
     */
    public static void setInstructionAddressAtTop(int instructionAddress){
        setInstructionAddress(instructionAddress,instructions.size());
    }

    /**
     * set instruction address to new value
     * @param instructionAddress instruction that will be edited
     * @param value new address value
     */
    public static void setInstructionAddress(int instructionAddress, int value){
        instructions.get(instructionAddress).setAddress(value);
    }

    /**
     * Prints error message and exit compilation
     */
    public static void exitWithCompileError(String message)
    {
        System.err.println("Compile error: "+message);
        System.exit(1);
    }

    public ArrayList<Instruction> getInstructions() {
        return instructions;
    }

    public static SymbolTable getSymTab(){
        return symbolTable;
    }

}
