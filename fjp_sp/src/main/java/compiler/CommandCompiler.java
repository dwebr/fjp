package compiler;

import instruction.InstructionOperationType;
import instruction.InstructionType;
import symbolTable.SymbolTableItem;
import symbolTable.SymbolTableItemType;
import types.Value;
import types.command.*;
import types.enums.ExpressionType;
import types.enums.SymbolType;
import types.expression.Expression;
import types.expression.UnaryOperExpression;

import java.util.ArrayList;

public class CommandCompiler {

    Command command;

    public CommandCompiler(Command command) {
        this.command = command;
    }

    public void compile()
    {
        switch(command.getType()){
            case WHILE:
                compileWhile((WhileCommand) command);
                break;
            case DO_WHILE:
                compileDoWhile((WhileCommand) command);
                break;
            case REPEAT_UNTIL:
                compileRepeatUntil((WhileCommand) command);
                break;
            case FOR:
                compileFor((ForCommand) command);
                break;
            case IF:
                compileIf((IfCommand) command);
                break;
            case SWITCH:
                compileSwitch((SwitchCommand) command);
                break;
            case ASSIGMENT:
                compileAssigment((VarAssigmentCommand) command);
                break;
            case PROCEDURE_CALL:
                compileProcedureCall((ProcedureCallCommand) command);
                break;
        }
    }



    private void compileProcedureCall(ProcedureCallCommand command) {

        SymbolTableItem symbol = Compiler.getSymTab().getSymbol(command.getName());

        // check if procedure with this name exists
        if((symbol == null) || (symbol.getType() != SymbolTableItemType.PROCEDURE)){
            String message = "Calling procedure \""+command.getName() +"\", procedure with this name is not declared.";
            Compiler.exitWithCompileError(message);
            return;
        }

        // check parameters size
        if(command.getParameters().size() != symbol.getProcParameters().size()){
            String message = "Calling procedure \""+command.getName() +"\", provided "+command.getParameters().size()+" parameter instead of "+symbol.getProcParameters().size()+".";
            Compiler.exitWithCompileError(message);
            return;
        }

        // check parameters' data type and push on stack
        int parameterIndex = 0;
        for (Expression parameter : command.getParameters()) {
            new ExpressionCompiler(parameter).compile();
            SymbolType requiredType = symbol.getProcParameters().get(parameterIndex).getDataType();
            if(parameter.getResultType() != requiredType){
                String message = "Calling procedure \""+command.getName() +"\", parameter number "+(parameterIndex+1) +", provided "+parameter.getResultType()+" instead of "+requiredType+".";
                Compiler.exitWithCompileError(message);
            }
            parameterIndex++;
        }

        Compiler.putInstruction(InstructionType.CAL,0,symbol.getAddress());

        // remove proc parameters from stack
        int parametersSize = 0;
        for (Expression parameter : command.getParameters()) {
            parametersSize++;
            if(parameter.getResultType() == SymbolType.BOOLEAN_ADDR || parameter.getResultType() == SymbolType.INT_ADDR){
                parametersSize++;
            }
        }
        if(parametersSize > 0){
            Compiler.putInstruction(InstructionType.INT,0,-parametersSize);
        }

    }

    private void compileSwitch(SwitchCommand command) {
        // index of instructions that jumps behind the switch in case block
        ArrayList<Integer> caseEndJumpsAddresses = new ArrayList<>();

        for (Value key : command.getCases().keySet()) {

            // compile switch variable to case key
            new ExpressionCompiler(command.getExpression()).compile();

            // compare key value to switch value
            Compiler.putInstruction(InstructionType.LIT,0, key.toInt());
            Compiler.putInstruction(InstructionType.OPR, 0,InstructionOperationType.EQ.toInt());

            //jump to next case if not equal
            int jumpAddressNextCase = Compiler.putInstruction(InstructionType.JMC,0,0);
            //compile case body
            compileCommandBlock(command.getCases().get(key));
            //jump behind switch
            int jumpAddressSwitchEnd = Compiler.putInstruction(InstructionType.JMP,0,0);
            caseEndJumpsAddresses.add(jumpAddressSwitchEnd);
            //set jump behind case tu jump behind last instruction
            Compiler.setInstructionAddressAtTop(jumpAddressNextCase);
        }

        // default branch
        if(command.getDefaultCase() != null){
            compileCommandBlock(command.getDefaultCase());
        }

        // set jumps after switch to jump behind last instruction
        for (Integer caseEndJumpsAddress : caseEndJumpsAddresses) {
            Compiler.setInstructionAddressAtTop(caseEndJumpsAddress);
        }

    }

    private void compileFor(ForCommand command) {

        // initialization
        new CommandCompiler(command.getInitialAssigment()).compile();

        // expression
        int expressionAddress = Compiler.getNextInstructionIndex();
        new ExpressionCompiler(command.getExpression()).compile();
        if(command.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = "For loop, expression has to result in BOOLEAN but "+command.getExpression().getResultType()+" was provided.";
            Compiler.exitWithCompileError(message);
        }

        // behind for if false
        int jump1InstructionIndex = Compiler.putInstruction(InstructionType.JMC,0,0 );

        // body
        compileCommandBlock(command.getBody());

        // do step
        new CommandCompiler(command.getStep()).compile();

        // jump at expression
        int jump2InstructionIndex = Compiler.putInstruction(InstructionType.JMP,0,0 );

        // set jump behind for to jump behind last instruction
        Compiler.setInstructionAddressAtTop(jump1InstructionIndex);

        // set expression jump to expression address
        Compiler.setInstructionAddress(jump2InstructionIndex,expressionAddress);

    }

    private void compileRepeatUntil(WhileCommand command) {
        int whileStartAddress = Compiler.getNextInstructionIndex();

        compileCommandBlock(command.getBody());

        new ExpressionCompiler(command.getExpression()).compile();
        if(command.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = "Repeat until loop, expression has to result in BOOLEAN but "+command.getExpression().getResultType()+" was provided.";
            Compiler.exitWithCompileError(message);
        }

        //jump back if false
        Compiler.putInstruction(InstructionType.JMC,0,whileStartAddress );
    }

    private void compileDoWhile(WhileCommand command) {

        int whileStartAddress = Compiler.getNextInstructionIndex();

        compileCommandBlock(command.getBody());

        // negation of the expression
        new ExpressionCompiler(new UnaryOperExpression(ExpressionType.NEGATION,command.getExpression())).compile();
        if(command.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = "Do while loop, expression has to result in BOOLEAN but "+command.getExpression().getResultType()+" was provided.";
            Compiler.exitWithCompileError(message);
        }

        // jump back if false
        Compiler.putInstruction(InstructionType.JMC,0,whileStartAddress );
    }

    private void compileWhile(WhileCommand command) {

        int expressionAddress = Compiler.getNextInstructionIndex();
        new ExpressionCompiler(command.getExpression()).compile();
        if(command.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = " While loop, expression has to result in BOOLEAN but "+command.getExpression().getResultType()+" was provided.";
            Compiler.exitWithCompileError(message);
        }

        // jump behind the while
        int jump1InstructionIndex = Compiler.putInstruction(InstructionType.JMC,0,0 );

        compileCommandBlock(command.getBody());

        // jump back to expression
        int jump2InstructionIndex = Compiler.putInstruction(InstructionType.JMP,0,0 );

        // set jump behind while to jump behind last instruction
        Compiler.setInstructionAddressAtTop(jump1InstructionIndex);

        // set expression jump to expression address
        Compiler.setInstructionAddress(jump2InstructionIndex,expressionAddress);
    }

    private void compileAssigment(VarAssigmentCommand command){
        // find symbol in symbol table
        SymbolTableItem symbol = Compiler.getSymTab().getSymbolFromTopLevel(command.getName());

        if(symbol == null || symbol.getType() == SymbolTableItemType.PROCEDURE){
            String message = "Assigning variable \""+command.getName() +"\", variable is no declared at this level.";
            Compiler.exitWithCompileError(message);
            return;
        }
        if(symbol.getType() == SymbolTableItemType.CONSTANT){
            String message = "Assigning constant \""+command.getName() +"\", constant cannot be reassigned.";
            Compiler.exitWithCompileError(message);
            return;
        }

        // compile expression and check data type
        new ExpressionCompiler(command.getExpression()).compile();

        if(command.getExpression().getResultType() != symbol.getDataType()){

            // references can be assigned with value that will be stored at the reference pointer
            if(!((command.getExpression().getResultType() == SymbolType.INT && symbol.getDataType() == SymbolType.INT_ADDR) ||
                    (command.getExpression().getResultType() == SymbolType.BOOLEAN && symbol.getDataType() == SymbolType.BOOLEAN_ADDR))){

                String message = "Assigning variable \""+command.getName() +"\", requested type is "+symbol.getDataType()+" but "+command.getExpression().getResultType()+" was provided.";
                Compiler.exitWithCompileError(message);

            }
        }else if(symbol.isAddress()){
            // references cannot be reassigned with address only its value can be chanced by another value
            String message = "Assigning variable \""+command.getName() +"\", reassigning address with address is not allowed.";
            Compiler.exitWithCompileError(message);
        }

        if(symbol.isAddress()){
            // if symbol is address load symbol level and address at top and dynamically store to that address
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress());
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress()+1);
            Compiler.putInstruction(InstructionType.PST,0, 0);

        }else{
            Compiler.putInstruction(InstructionType.STO,0, symbol.getAddress());
        }

    }

    private void compileIf(IfCommand command){
        // compile and check expression data type
        new ExpressionCompiler(command.getExpression()).compile();
        if(command.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = "If command, expression has to result in BOOLEAN but "+command.getExpression().getResultType()+" was provided.";
            Compiler.exitWithCompileError(message);
        }

        // jump to else
        int jump1InstructionIndex = Compiler.putInstruction(InstructionType.JMC,0,0 );

        //commands if true
        compileCommandBlock(command.getCommandsIfTrue());

        // jump behind else
        int jump2InstructionIndex = Compiler.putInstruction(InstructionType.JMP,0,0 );

        // set jump to else to jump behind last instruction
        Compiler.setInstructionAddressAtTop(jump1InstructionIndex);

        //commands if false
        compileCommandBlock(command.getCommandsIfFalse());

        // set jump behind else to jump behind last instruction
        Compiler.setInstructionAddressAtTop(jump2InstructionIndex);

    }

    private void compileCommandBlock( BlockCommand commands){
        for (Command command : commands.getCommands()) {
            new CommandCompiler(command).compile();
        }
    }
}
