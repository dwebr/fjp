package compiler;

import instruction.InstructionType;
import symbolTable.SymbolTableItem;
import symbolTable.SymbolTableItemType;
import types.Block;
import types.Symbol;
import types.command.BlockCommand;
import types.command.Command;
import types.procedure.Procedure;

import java.util.List;

/**
 * Block can exist as a procedure body or as program root block
 */
public class BlockCompiler{

    private final Block block;

    // parameters of procedure, will be set only if the block is procedure body, not the root block of program
    private List<Symbol> parameters = null;

    // required size of parameters on stack
    private int parametersSize = 0;

    public BlockCompiler(Block block) {
        this.block = block;
    }

    public BlockCompiler(Block block, List<Symbol> parameters) {
        this.block = block;
        this.parameters = parameters;
        for (Symbol symbol : this.parameters) {
            // address parameters have size on stack 2
            // others have size 1
            parametersSize+=symbol.getSize();
        }
    }


    public void compile()
    {
        // add new level to symbol table
        Compiler.getSymTab().addLevel();

        //stack increment for const, variables and parameters
        int stackIncrement = Compiler.BLOCK_DEFAULT_STACK_SIZE + block.getSymbols().size() + parametersSize;
        Compiler.putInstruction(InstructionType.INT,0,stackIncrement);

        // stores parameters on stack
        storeParameters(parameters);

        // variable and constants declarations
        compileSymbolDeclarations(block.getSymbols());

        // procedures
        if(block.getProcedures().size() > 0){
            int procSkipAddress = Compiler.putInstruction(InstructionType.JMP,0,0);
            compileProcDeclarations(block.getProcedures());
            Compiler.setInstructionAddressAtTop(procSkipAddress);
        }

        // commands
        compileCommands( block.getCommands());

        // remove level from symbol table
        Compiler.getSymTab().removeLevel();

        // return instruction
        Compiler.putInstruction(InstructionType.RET,0,0);
    }

    /**
     * stores parameters on stack and to the symbol table, needed if block is procedure body that has parameters
     */
    private void storeParameters(List<Symbol> parameters) {
        if(parameters == null){
            return;
        }
        int index = 0;
        for (Symbol parameter : parameters) {
            //loads on top of a stack and stores to initialized space
            Compiler.putInstruction(InstructionType.LOD,0,index-parametersSize);
            Compiler.putInstruction(InstructionType.STO,0,Compiler.BLOCK_DEFAULT_STACK_SIZE+index);

            SymbolTableItem item = new SymbolTableItem(parameter.getName(),Compiler.BLOCK_DEFAULT_STACK_SIZE+index,SymbolTableItemType.VARIABLE,parameter.getDataType());

            // if its address it has 2 values (lvl,addr), so another store has to be done
            if(parameter.isAddress()){
                index++;
                Compiler.putInstruction(InstructionType.LOD,0,index-parametersSize);
                Compiler.putInstruction(InstructionType.STO,0,Compiler.BLOCK_DEFAULT_STACK_SIZE+index);
            }

            Compiler.getSymTab().addSymbolAtTopLevel(parameter.getName(), item);

            index++;
        }
    }

    /**
     * compiles procedure declarations
     */
    private void compileProcDeclarations(List<Procedure> procedures) {
        for (Procedure proc: procedures){
            //check if symbol already exists at the same level
            if (Compiler.getSymTab().existsSymbolAtTopLevel(proc.getName()))
            {
                String message = "Declaring procedure \""+proc.getName() +"\", there is already declared symbol with the same name at the same level.";
                Compiler.exitWithCompileError(message);
            }

            SymbolTableItem item = new SymbolTableItem(proc.getName(),Compiler.getNextInstructionIndex(),SymbolTableItemType.PROCEDURE,proc.getParameters());

            Compiler.getSymTab().addSymbolAtTopLevel(proc.getName(), item);

            //compile procedure body
            new BlockCompiler(proc.getBlock(),proc.getParameters()).compile();
        }
    }

    /**
     * compiles variables and constants declarations
     */
    private void compileSymbolDeclarations(List<Symbol> symbols){
        int index = parametersSize;
        for (Symbol symbol: symbols){
            //check if symbol already exists at the same level
            if (Compiler.getSymTab().existsSymbolAtTopLevel(symbol.getName()))
            {
                String message = "Declaring constant or variable \""+symbol.getName() +"\", there is already declared symbol with the same name at the same level.";
                Compiler.exitWithCompileError(message);
            }

            if(symbol.getExpression() != null){
                // check if provided data type is correct and store value to stack
                new ExpressionCompiler(symbol.getExpression()).compile();
                if(symbol.getExpression().getResultType() != symbol.getDataType()){
                    String message = "Declaring constant or variable \""+symbol.getName() +"\", expected "+symbol.getDataType()+ " data type but provided "+symbol.getExpression().getResultType();
                    Compiler.exitWithCompileError(message);
                }
                Compiler.putInstruction(InstructionType.STO,0,Compiler.BLOCK_DEFAULT_STACK_SIZE + index);
            }

            //store to symbol table
            SymbolTableItemType symbolType = symbol.isConstant()? SymbolTableItemType.CONSTANT : SymbolTableItemType.VARIABLE;
            SymbolTableItem item = new SymbolTableItem(symbol.getName(),Compiler.BLOCK_DEFAULT_STACK_SIZE +index,symbolType,symbol.getDataType());
            Compiler.getSymTab().addSymbolAtTopLevel(symbol.getName(), item);
            index ++;
        }
    }


    private void compileCommands(BlockCommand commands){
        for (Command command : commands.getCommands()) {
            new CommandCompiler(command).compile();
        }
    }
}
