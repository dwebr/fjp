package compiler;

import instruction.InstructionOperationType;
import instruction.InstructionType;
import symbolTable.SymbolTableItem;
import symbolTable.SymbolTableItemType;
import types.enums.ExpressionType;
import types.enums.SymbolType;
import types.expression.BinaryOperExpression;
import types.expression.Expression;
import types.expression.UnaryOperExpression;
import types.expression.ValueExpression;

public class ExpressionCompiler {

    Expression expression;

    public ExpressionCompiler(Expression expression) {
        this.expression = expression;
    }

    public void compile()
    {
        switch(expression.getType()) {
            case VALUE:
                compileValueExpression((ValueExpression) expression);
                break;
            case IDENT:
                compileIdentExpression((ValueExpression) expression);
                break;
            case MULT:
            case DIV:
            case PLUS:
            case MINUS:
                compileArithmeticExpression((BinaryOperExpression) expression);
                break;
            case EQ:
            case NOT_EQ:
            case LT:
            case LE:
            case GE:
            case GT:
                compileComparisonExpression((BinaryOperExpression)  expression);
                break;
            case AND:
            case OR:
                compileLogicalOperExpression((BinaryOperExpression)  expression);
                break;
            case PAREN:
                compileParenExpression((UnaryOperExpression) expression);
                break;
            case NEGATION:
                compileNegationExpression((UnaryOperExpression) expression);
                break;
            case PLUS_BEFORE:
            case MINUS_BEFORE:
                compileSignBeforeExpression((UnaryOperExpression) expression);
                break;
            case IDENT_ADDRESS:
                compileIdentAddressExpression((ValueExpression) expression);
                break;
        }
    }

    /**
     *     expression in proc call p(a#), meaning reference to variable "a" is given
     */
    private void compileIdentAddressExpression(ValueExpression expression) {
        // Search for symbol in symbol table and check data type
        SymbolTableItem symbol = Compiler.getSymTab().getSymbolFromTopLevel(expression.getValue().toString());
        if(symbol == null){
            String message = "Expression variable \""+expression.getValue().toString() +"#\", variable is not declared at this level.";
            Compiler.exitWithCompileError(message);
            return;
        }
        if(symbol.getType() == SymbolTableItemType.PROCEDURE || symbol.getType() == SymbolTableItemType.CONSTANT ){
            String message = "Expression identifier \""+expression.getValue().toString() +"#\", has to be a variable.";
            Compiler.exitWithCompileError(message);
        }

        // put level and address at the top of stack
        if(symbol.isAddress() ){
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress());
            //increase level by 1
            Compiler.putInstruction(InstructionType.LIT,0,1);
            Compiler.putInstruction(InstructionType.OPR,0,InstructionOperationType.PLUS.toInt());
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress()+1);
        }else{
            Compiler.putInstruction(InstructionType.LIT,0,1);
            Compiler.putInstruction(InstructionType.LIT,0,symbol.getAddress());
        }

        // set result type
        if(symbol.getDataType() == SymbolType.BOOLEAN){
            expression.setResultType(SymbolType.BOOLEAN_ADDR);
        }else if(symbol.getDataType() == SymbolType.INT){
            expression.setResultType(SymbolType.INT_ADDR);
        }else{
            expression.setResultType(symbol.getDataType());
        }
    }

    private void compileSignBeforeExpression(UnaryOperExpression expression) {
        new ExpressionCompiler(expression.getExpression()).compile();
        if( expression.getExpression().getResultType() != SymbolType.INT){
            String message = "Sign before expression  \""+expression.getType()  +"\", inner expression has to be an INT.";
            Compiler.exitWithCompileError(message);
        }

        if(expression.getType() == ExpressionType.MINUS_BEFORE){
            Compiler.putInstruction(InstructionType.OPR, 0, InstructionOperationType.UNARY_MINUS.toInt());
        }

        expression.setResultType(SymbolType.INT);
    }

    private void compileNegationExpression(UnaryOperExpression expression) {
        new ExpressionCompiler(expression.getExpression()).compile();

        if( expression.getExpression().getResultType() != SymbolType.BOOLEAN){
            String message = "Negation expression, inner expression has to be an BOOLEAN.";
            Compiler.exitWithCompileError(message);
        }

        // equal with 0
        Compiler.putInstruction(InstructionType.LIT, 0,0);
        Compiler.putInstruction(InstructionType.OPR, 0, InstructionOperationType.EQ.toInt());

        expression.setResultType(SymbolType.BOOLEAN);
    }

    /**
     * expression in parentis ()
     */
    private void compileParenExpression(UnaryOperExpression expression) {
        new ExpressionCompiler(expression.getExpression()).compile();
        expression.setResultType(expression.getExpression().getResultType());
    }

    private void compileLogicalOperExpression(BinaryOperExpression expression) {

        // compile expressions and check if resultType is boolean
        new ExpressionCompiler(expression.getExpression1()).compile();
        new ExpressionCompiler(expression.getExpression2()).compile();
        if( (expression.getExpression1().getResultType() != SymbolType.BOOLEAN) || (expression.getExpression2().getResultType() != SymbolType.BOOLEAN)){
            String message = "Logical operation \""+expression.getType() +"\", expected BOOLEAN operands but provided "+expression.getExpression1().getResultType()+" and "+expression.getExpression2().getResultType();
            Compiler.exitWithCompileError(message);
        }

        switch(expression.getType()){
            case AND:
                // multiplication
                Compiler.putInstruction(InstructionType.OPR,0,InstructionOperationType.MULT.toInt());
                break;
            case OR:
                // addition and greater than zero
                Compiler.putInstruction(InstructionType.OPR,0,InstructionOperationType.PLUS.toInt());
                Compiler.putInstruction(InstructionType.LIT,0,0);
                Compiler.putInstruction(InstructionType.OPR,0,InstructionOperationType.GT.toInt());
                break;
        }

        expression.setResultType(SymbolType.BOOLEAN);
    }


    private void compileIdentExpression(ValueExpression expression) {

        // load from symbol table and check existence and type
        SymbolTableItem symbol = Compiler.getSymTab().getSymbolFromTopLevel(expression.getValue().toString());
        if(symbol == null || symbol.getType() == SymbolTableItemType.PROCEDURE){
            String message = "Identifier in expression \""+expression.getValue().toString() +"\" is not declared at this level.";
            Compiler.exitWithCompileError(message);
            return;
        }

        // if address load from stored level and address
        if(symbol.isAddress()){
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress());
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress()+1);
            Compiler.putInstruction(InstructionType.PLD, 0, 0);
        }else{
            Compiler.putInstruction(InstructionType.LOD, 0, symbol.getAddress());
        }

        // set result tape, change result type if identifier point to address to its value
        if(symbol.getDataType() == SymbolType.BOOLEAN_ADDR){
            expression.setResultType(SymbolType.BOOLEAN);
        }else if(symbol.getDataType() == SymbolType.INT_ADDR){
            expression.setResultType(SymbolType.INT);
        }else{
            expression.setResultType(symbol.getDataType());
        }

    }

    /**
     * literal in expression
     */
    private void compileValueExpression(ValueExpression expression){
        Compiler.putInstruction(InstructionType.LIT,0,expression.getValue().toInt());
        expression.setResultType(expression.getValue().getType());
    }

    private void compileComparisonExpression(BinaryOperExpression expression){

        // compile expression and check their type
        new ExpressionCompiler(expression.getExpression1()).compile();
        new ExpressionCompiler(expression.getExpression2()).compile();
        if( (expression.getExpression1().getResultType() != SymbolType.INT) || (expression.getExpression2().getResultType() != SymbolType.INT)){
            String message = "Compare operation \""+expression.getType() +"\", expected INT operands but provided "+expression.getExpression1().getResultType()+" and "+expression.getExpression2().getResultType();
            Compiler.exitWithCompileError(message);
        }

        int operationType = 0;

        switch(expression.getType()){
            case EQ:
                operationType = InstructionOperationType.EQ.toInt();
                break;
            case NOT_EQ:
                operationType = InstructionOperationType.NOT_EQ.toInt();
                break;
            case LT:
                operationType = InstructionOperationType.LT.toInt();
                break;
            case LE:
                operationType = InstructionOperationType.LE.toInt();
                break;
            case GE:
                operationType = InstructionOperationType.GE.toInt();
                break;
            case GT:
                operationType = InstructionOperationType.GT.toInt();
                break;
        }

        Compiler.putInstruction(InstructionType.OPR,0,operationType);

        expression.setResultType(SymbolType.BOOLEAN);
    }

    private void compileArithmeticExpression(BinaryOperExpression expression){

        // compile expressions and check their data types
        new ExpressionCompiler(expression.getExpression1()).compile();
        new ExpressionCompiler(expression.getExpression2()).compile();
        if( (expression.getExpression1().getResultType() != SymbolType.INT) || (expression.getExpression2().getResultType() != SymbolType.INT)){
            String message = "Arithmetic operation \""+expression.getType() +"\", expected INT operands but provided "+expression.getExpression1().getResultType()+" and "+expression.getExpression2().getResultType();
            Compiler.exitWithCompileError(message);
        }

        int operationType = 0;

        switch(expression.getType()){
            case MULT:
                operationType = InstructionOperationType.MULT.toInt();
                break;
            case DIV:
                operationType = InstructionOperationType.DIV.toInt();
                break;
            case PLUS:
                operationType = InstructionOperationType.PLUS.toInt();
                break;
            case MINUS:
                operationType = InstructionOperationType.MINUS.toInt();
                break;
        }

        Compiler.putInstruction(InstructionType.OPR,0,operationType);

        expression.setResultType(SymbolType.INT);
    }


}

