
import errorHandlers.ErrorListener;
import compiler.Compiler;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import gen.GramLexer;
import gen.GramParser;
import instruction.Instruction;
import types.Program;
import visitors.ProgramVisitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // check arguments counts
        if (args.length != 2)
        {
            System.out.println("Invalid arguments, format is: inputFile outputFile");
            System.exit(1);
        }

        // reading input file
        CharStream inputStream = null;
        try {
            inputStream = CharStreams.fromFileName(args[0]);

        } catch (Exception e) {
            System.out.println("Input file not found " + args[0]);
        }

        // error listener producing formatted error messages for lexer and parser
        ErrorListener errorListener = new ErrorListener();

        // lexing part
        GramLexer lexer = new GramLexer(inputStream);
        lexer.removeErrorListeners();
        lexer.addErrorListener(errorListener);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // parsing part
        GramParser parser = new GramParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.setBuildParseTree(true);
        ParseTree parseTree = parser.program();

        // using visitor to create instance of Program class, representing program code as derivation tree
        Program program = new ProgramVisitor().visit(parseTree);

        // compiling program -> syntax analysis and instruction generation
        Compiler compiler = new Compiler(program);
        compiler.compile();

        // write instruction into an output file
        writeInstructions(args[1], compiler.getInstructions());

        System.out.println("Program was successfully compiled into "+args[1]);

    }

    /**
     * Writes instructions into a file
     * @param outputFile file
     * @param instructions instructions list
     */
    private static void writeInstructions(String outputFile, List<Instruction> instructions)
    {
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(outputFile, StandardCharsets.UTF_8);
            int lineIndex = 0;
            for (Instruction instruction: instructions)
            {
                writer.write(lineIndex + instruction.toString());
                lineIndex++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert writer != null;
            writer.close();
        }

    }

}