package gen;// Generated from C:/Users/dandu/Documents/skola/FJP/fjp/fjp_sp/src\Gram.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GramParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GramVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GramParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(GramParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(GramParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#blockConst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockConst(GramParser.BlockConstContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#blockVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockVar(GramParser.BlockVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#blockProc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockProc(GramParser.BlockProcContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfCommand(GramParser.IfCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForCommand(GramParser.ForCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileCommand(GramParser.WhileCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doWhileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileCommand(GramParser.DoWhileCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repeatUntilCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeatUntilCommand(GramParser.RepeatUntilCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code switchCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchCommand(GramParser.SwitchCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code procedureCallCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcedureCallCommand(GramParser.ProcedureCallCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableAssigmentCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableAssigmentCommand(GramParser.VariableAssigmentCommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#blockCommand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockCommand(GramParser.BlockCommandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegationExpression(GramParser.NegationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valueExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValueExpression(GramParser.ValueExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plusMinusExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusMinusExpression(GramParser.PlusMinusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plusMinusBeforeExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusMinusBeforeExpression(GramParser.PlusMinusBeforeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompExpression(GramParser.CompExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andOrExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOrExpression(GramParser.AndOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multDivExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultDivExpression(GramParser.MultDivExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpression(GramParser.ParenExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identAddressExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentAddressExpression(GramParser.IdentAddressExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentExpression(GramParser.IdentExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#procedureCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcedureCall(GramParser.ProcedureCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#possibleTypes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPossibleTypes(GramParser.PossibleTypesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#possibleValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPossibleValues(GramParser.PossibleValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#variableAssigment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableAssigment(GramParser.VariableAssigmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#forControl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForControl(GramParser.ForControlContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#switchCases}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchCases(GramParser.SwitchCasesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#switchDefaultCase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchDefaultCase(GramParser.SwitchDefaultCaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#symbol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbol(GramParser.SymbolContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#symbolDeclAssign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbolDeclAssign(GramParser.SymbolDeclAssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramParser#procParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcParameter(GramParser.ProcParameterContext ctx);
}