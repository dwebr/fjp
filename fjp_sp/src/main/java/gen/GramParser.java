package gen;// Generated from C:/Users/dandu/Documents/skola/FJP/fjp/fjp_sp/src\Gram.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GramParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		CONST_KEYWORD=18, VARIABLE_KEYWORD=19, PROCEDURE_KEYWORD=20, ADDRESS_KEYWORD=21, 
		BOOLEAN=22, INT=23, STRING=24, BOOLEAN_LITERAL=25, ASSIGN=26, END_STATEMENT=27, 
		PLUS_MINUS=28, MULT_DIV=29, AND_OR=30, NEGATION=31, LOG_COMPARISONS=32, 
		IDENT=33, INT_LITERAL=34, WHITESPACE=35, LINE_COMMENT=36;
	public static final int
		RULE_program = 0, RULE_block = 1, RULE_blockConst = 2, RULE_blockVar = 3, 
		RULE_blockProc = 4, RULE_command = 5, RULE_blockCommand = 6, RULE_expression = 7, 
		RULE_procedureCall = 8, RULE_possibleTypes = 9, RULE_possibleValues = 10, 
		RULE_variableAssigment = 11, RULE_forControl = 12, RULE_switchCases = 13, 
		RULE_switchDefaultCase = 14, RULE_symbol = 15, RULE_symbolDeclAssign = 16, 
		RULE_procParameter = 17;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "block", "blockConst", "blockVar", "blockProc", "command", 
			"blockCommand", "expression", "procedureCall", "possibleTypes", "possibleValues", 
			"variableAssigment", "forControl", "switchCases", "switchDefaultCase", 
			"symbol", "symbolDeclAssign", "procParameter"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'.'", "','", "'('", "')'", "'{'", "'}'", "'if'", "'else'", "'for'", 
			"'while'", "'do'", "'repeat'", "'until'", "'switch'", "'case'", "':'", 
			"'default'", "'const'", "'var'", "'proc'", "'#'", "'boolean'", "'int'", 
			"'string'", null, "'='", "';'", null, null, null, "'!'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "CONST_KEYWORD", "VARIABLE_KEYWORD", 
			"PROCEDURE_KEYWORD", "ADDRESS_KEYWORD", "BOOLEAN", "INT", "STRING", "BOOLEAN_LITERAL", 
			"ASSIGN", "END_STATEMENT", "PLUS_MINUS", "MULT_DIV", "AND_OR", "NEGATION", 
			"LOG_COMPARISONS", "IDENT", "INT_LITERAL", "WHITESPACE", "LINE_COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Gram.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GramParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(36);
			block();
			setState(37);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public BlockConstContext blockConst() {
			return getRuleContext(BlockConstContext.class,0);
		}
		public BlockVarContext blockVar() {
			return getRuleContext(BlockVarContext.class,0);
		}
		public List<BlockProcContext> blockProc() {
			return getRuleContexts(BlockProcContext.class);
		}
		public BlockProcContext blockProc(int i) {
			return getRuleContext(BlockProcContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST_KEYWORD) {
				{
				setState(39);
				blockConst();
				}
			}

			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VARIABLE_KEYWORD) {
				{
				setState(42);
				blockVar();
				}
			}

			setState(48);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PROCEDURE_KEYWORD) {
				{
				{
				setState(45);
				blockProc();
				}
				}
				setState(50);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(51);
			blockCommand();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockConstContext extends ParserRuleContext {
		public TerminalNode CONST_KEYWORD() { return getToken(GramParser.CONST_KEYWORD, 0); }
		public List<SymbolDeclAssignContext> symbolDeclAssign() {
			return getRuleContexts(SymbolDeclAssignContext.class);
		}
		public SymbolDeclAssignContext symbolDeclAssign(int i) {
			return getRuleContext(SymbolDeclAssignContext.class,i);
		}
		public TerminalNode END_STATEMENT() { return getToken(GramParser.END_STATEMENT, 0); }
		public BlockConstContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockConst; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterBlockConst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitBlockConst(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitBlockConst(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockConstContext blockConst() throws RecognitionException {
		BlockConstContext _localctx = new BlockConstContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_blockConst);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			match(CONST_KEYWORD);
			setState(54);
			symbolDeclAssign();
			setState(59);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(55);
				match(T__1);
				setState(56);
				symbolDeclAssign();
				}
				}
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(62);
			match(END_STATEMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockVarContext extends ParserRuleContext {
		public TerminalNode VARIABLE_KEYWORD() { return getToken(GramParser.VARIABLE_KEYWORD, 0); }
		public List<SymbolContext> symbol() {
			return getRuleContexts(SymbolContext.class);
		}
		public SymbolContext symbol(int i) {
			return getRuleContext(SymbolContext.class,i);
		}
		public TerminalNode END_STATEMENT() { return getToken(GramParser.END_STATEMENT, 0); }
		public BlockVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterBlockVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitBlockVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitBlockVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockVarContext blockVar() throws RecognitionException {
		BlockVarContext _localctx = new BlockVarContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_blockVar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(VARIABLE_KEYWORD);
			setState(65);
			symbol();
			setState(70);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(66);
				match(T__1);
				setState(67);
				symbol();
				}
				}
				setState(72);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(73);
			match(END_STATEMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockProcContext extends ParserRuleContext {
		public TerminalNode PROCEDURE_KEYWORD() { return getToken(GramParser.PROCEDURE_KEYWORD, 0); }
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<ProcParameterContext> procParameter() {
			return getRuleContexts(ProcParameterContext.class);
		}
		public ProcParameterContext procParameter(int i) {
			return getRuleContext(ProcParameterContext.class,i);
		}
		public BlockProcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockProc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterBlockProc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitBlockProc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitBlockProc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockProcContext blockProc() throws RecognitionException {
		BlockProcContext _localctx = new BlockProcContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_blockProc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(PROCEDURE_KEYWORD);
			setState(76);
			match(IDENT);
			setState(77);
			match(T__2);
			setState(86);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BOOLEAN || _la==INT) {
				{
				setState(78);
				procParameter();
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__1) {
					{
					{
					setState(79);
					match(T__1);
					setState(80);
					procParameter();
					}
					}
					setState(85);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(88);
			match(T__3);
			setState(89);
			match(T__4);
			setState(90);
			block();
			setState(91);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
	 
		public CommandContext() { }
		public void copyFrom(CommandContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ForCommandContext extends CommandContext {
		public ForControlContext forControl() {
			return getRuleContext(ForControlContext.class,0);
		}
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public ForCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterForCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitForCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitForCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProcedureCallCommandContext extends CommandContext {
		public ProcedureCallContext procedureCall() {
			return getRuleContext(ProcedureCallContext.class,0);
		}
		public TerminalNode END_STATEMENT() { return getToken(GramParser.END_STATEMENT, 0); }
		public ProcedureCallCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterProcedureCallCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitProcedureCallCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitProcedureCallCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepeatUntilCommandContext extends CommandContext {
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RepeatUntilCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterRepeatUntilCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitRepeatUntilCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitRepeatUntilCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SwitchCommandContext extends CommandContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<SwitchCasesContext> switchCases() {
			return getRuleContexts(SwitchCasesContext.class);
		}
		public SwitchCasesContext switchCases(int i) {
			return getRuleContext(SwitchCasesContext.class,i);
		}
		public SwitchDefaultCaseContext switchDefaultCase() {
			return getRuleContext(SwitchDefaultCaseContext.class,0);
		}
		public SwitchCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterSwitchCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitSwitchCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitSwitchCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileCommandContext extends CommandContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public WhileCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterWhileCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitWhileCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitWhileCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoWhileCommandContext extends CommandContext {
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DoWhileCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterDoWhileCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitDoWhileCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitDoWhileCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfCommandContext extends CommandContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<BlockCommandContext> blockCommand() {
			return getRuleContexts(BlockCommandContext.class);
		}
		public BlockCommandContext blockCommand(int i) {
			return getRuleContext(BlockCommandContext.class,i);
		}
		public IfCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterIfCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitIfCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitIfCommand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableAssigmentCommandContext extends CommandContext {
		public VariableAssigmentContext variableAssigment() {
			return getRuleContext(VariableAssigmentContext.class,0);
		}
		public TerminalNode END_STATEMENT() { return getToken(GramParser.END_STATEMENT, 0); }
		public VariableAssigmentCommandContext(CommandContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterVariableAssigmentCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitVariableAssigmentCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitVariableAssigmentCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_command);
		int _la;
		try {
			setState(160);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new IfCommandContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(93);
				match(T__6);
				setState(94);
				match(T__2);
				setState(95);
				expression(0);
				setState(96);
				match(T__3);
				setState(97);
				match(T__4);
				setState(98);
				blockCommand();
				setState(99);
				match(T__5);
				setState(105);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(100);
					match(T__7);
					setState(101);
					match(T__4);
					setState(102);
					blockCommand();
					setState(103);
					match(T__5);
					}
				}

				}
				break;
			case 2:
				_localctx = new ForCommandContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(107);
				match(T__8);
				setState(108);
				forControl();
				setState(109);
				match(T__4);
				setState(110);
				blockCommand();
				setState(111);
				match(T__5);
				}
				break;
			case 3:
				_localctx = new WhileCommandContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(113);
				match(T__9);
				setState(114);
				match(T__2);
				setState(115);
				expression(0);
				setState(116);
				match(T__3);
				setState(117);
				match(T__4);
				setState(118);
				blockCommand();
				setState(119);
				match(T__5);
				}
				break;
			case 4:
				_localctx = new DoWhileCommandContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(121);
				match(T__10);
				setState(122);
				match(T__4);
				setState(123);
				blockCommand();
				setState(124);
				match(T__5);
				setState(125);
				match(T__9);
				setState(126);
				match(T__2);
				setState(127);
				expression(0);
				setState(128);
				match(T__3);
				}
				break;
			case 5:
				_localctx = new RepeatUntilCommandContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(130);
				match(T__11);
				setState(131);
				match(T__4);
				setState(132);
				blockCommand();
				setState(133);
				match(T__5);
				setState(134);
				match(T__12);
				setState(135);
				match(T__2);
				setState(136);
				expression(0);
				setState(137);
				match(T__3);
				}
				break;
			case 6:
				_localctx = new SwitchCommandContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(139);
				match(T__13);
				setState(140);
				match(T__2);
				setState(141);
				expression(0);
				setState(142);
				match(T__3);
				setState(143);
				match(T__4);
				setState(145); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(144);
					switchCases();
					}
					}
					setState(147); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__14 );
				setState(150);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__16) {
					{
					setState(149);
					switchDefaultCase();
					}
				}

				setState(152);
				match(T__5);
				}
				break;
			case 7:
				_localctx = new ProcedureCallCommandContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(154);
				procedureCall();
				setState(155);
				match(END_STATEMENT);
				}
				break;
			case 8:
				_localctx = new VariableAssigmentCommandContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(157);
				variableAssigment();
				setState(158);
				match(END_STATEMENT);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockCommandContext extends ParserRuleContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public BlockCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterBlockCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitBlockCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitBlockCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockCommandContext blockCommand() throws RecognitionException {
		BlockCommandContext _localctx = new BlockCommandContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_blockCommand);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__13) | (1L << IDENT))) != 0)) {
				{
				{
				setState(162);
				command();
				}
				}
				setState(167);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NegationExpressionContext extends ExpressionContext {
		public TerminalNode NEGATION() { return getToken(GramParser.NEGATION, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NegationExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterNegationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitNegationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitNegationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValueExpressionContext extends ExpressionContext {
		public PossibleValuesContext possibleValues() {
			return getRuleContext(PossibleValuesContext.class,0);
		}
		public ValueExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterValueExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitValueExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitValueExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusMinusExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PLUS_MINUS() { return getToken(GramParser.PLUS_MINUS, 0); }
		public PlusMinusExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterPlusMinusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitPlusMinusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitPlusMinusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusMinusBeforeExpressionContext extends ExpressionContext {
		public TerminalNode PLUS_MINUS() { return getToken(GramParser.PLUS_MINUS, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PlusMinusBeforeExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterPlusMinusBeforeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitPlusMinusBeforeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitPlusMinusBeforeExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CompExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode LOG_COMPARISONS() { return getToken(GramParser.LOG_COMPARISONS, 0); }
		public CompExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterCompExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitCompExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitCompExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndOrExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode AND_OR() { return getToken(GramParser.AND_OR, 0); }
		public AndOrExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterAndOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitAndOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitAndOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultDivExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MULT_DIV() { return getToken(GramParser.MULT_DIV, 0); }
		public MultDivExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterMultDivExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitMultDivExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitMultDivExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExpressionContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParenExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterParenExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitParenExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitParenExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentAddressExpressionContext extends ExpressionContext {
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public TerminalNode ADDRESS_KEYWORD() { return getToken(GramParser.ADDRESS_KEYWORD, 0); }
		public IdentAddressExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterIdentAddressExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitIdentAddressExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitIdentAddressExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentExpressionContext extends ExpressionContext {
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public IdentExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterIdentExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitIdentExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitIdentExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(181);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				_localctx = new ValueExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(169);
				possibleValues();
				}
				break;
			case 2:
				{
				_localctx = new IdentExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(170);
				match(IDENT);
				}
				break;
			case 3:
				{
				_localctx = new ParenExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(171);
				match(T__2);
				setState(172);
				expression(0);
				setState(173);
				match(T__3);
				}
				break;
			case 4:
				{
				_localctx = new NegationExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(175);
				match(NEGATION);
				setState(176);
				expression(3);
				}
				break;
			case 5:
				{
				_localctx = new PlusMinusBeforeExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(177);
				match(PLUS_MINUS);
				setState(178);
				expression(2);
				}
				break;
			case 6:
				{
				_localctx = new IdentAddressExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(179);
				match(IDENT);
				setState(180);
				match(ADDRESS_KEYWORD);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(197);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(195);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
					case 1:
						{
						_localctx = new MultDivExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(183);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(184);
						match(MULT_DIV);
						setState(185);
						expression(9);
						}
						break;
					case 2:
						{
						_localctx = new PlusMinusExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(186);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(187);
						match(PLUS_MINUS);
						setState(188);
						expression(8);
						}
						break;
					case 3:
						{
						_localctx = new CompExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(189);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(190);
						match(LOG_COMPARISONS);
						setState(191);
						expression(7);
						}
						break;
					case 4:
						{
						_localctx = new AndOrExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(192);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(193);
						match(AND_OR);
						setState(194);
						expression(6);
						}
						break;
					}
					} 
				}
				setState(199);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ProcedureCallContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ProcedureCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterProcedureCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitProcedureCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitProcedureCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProcedureCallContext procedureCall() throws RecognitionException {
		ProcedureCallContext _localctx = new ProcedureCallContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_procedureCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(IDENT);
			setState(201);
			match(T__2);
			setState(210);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << BOOLEAN_LITERAL) | (1L << PLUS_MINUS) | (1L << NEGATION) | (1L << IDENT) | (1L << INT_LITERAL))) != 0)) {
				{
				setState(202);
				expression(0);
				setState(207);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__1) {
					{
					{
					setState(203);
					match(T__1);
					setState(204);
					expression(0);
					}
					}
					setState(209);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(212);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PossibleTypesContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(GramParser.INT, 0); }
		public TerminalNode BOOLEAN() { return getToken(GramParser.BOOLEAN, 0); }
		public PossibleTypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_possibleTypes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterPossibleTypes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitPossibleTypes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitPossibleTypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PossibleTypesContext possibleTypes() throws RecognitionException {
		PossibleTypesContext _localctx = new PossibleTypesContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_possibleTypes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			_la = _input.LA(1);
			if ( !(_la==BOOLEAN || _la==INT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PossibleValuesContext extends ParserRuleContext {
		public TerminalNode INT_LITERAL() { return getToken(GramParser.INT_LITERAL, 0); }
		public TerminalNode PLUS_MINUS() { return getToken(GramParser.PLUS_MINUS, 0); }
		public TerminalNode BOOLEAN_LITERAL() { return getToken(GramParser.BOOLEAN_LITERAL, 0); }
		public PossibleValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_possibleValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterPossibleValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitPossibleValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitPossibleValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PossibleValuesContext possibleValues() throws RecognitionException {
		PossibleValuesContext _localctx = new PossibleValuesContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_possibleValues);
		int _la;
		try {
			setState(221);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS_MINUS:
			case INT_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(217);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS_MINUS) {
					{
					setState(216);
					match(PLUS_MINUS);
					}
				}

				setState(219);
				match(INT_LITERAL);
				}
				}
				break;
			case BOOLEAN_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(220);
				match(BOOLEAN_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableAssigmentContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public TerminalNode ASSIGN() { return getToken(GramParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableAssigmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableAssigment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterVariableAssigment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitVariableAssigment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitVariableAssigment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableAssigmentContext variableAssigment() throws RecognitionException {
		VariableAssigmentContext _localctx = new VariableAssigmentContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_variableAssigment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			match(IDENT);
			setState(224);
			match(ASSIGN);
			setState(225);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForControlContext extends ParserRuleContext {
		public List<VariableAssigmentContext> variableAssigment() {
			return getRuleContexts(VariableAssigmentContext.class);
		}
		public VariableAssigmentContext variableAssigment(int i) {
			return getRuleContext(VariableAssigmentContext.class,i);
		}
		public List<TerminalNode> END_STATEMENT() { return getTokens(GramParser.END_STATEMENT); }
		public TerminalNode END_STATEMENT(int i) {
			return getToken(GramParser.END_STATEMENT, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForControlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forControl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterForControl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitForControl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitForControl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForControlContext forControl() throws RecognitionException {
		ForControlContext _localctx = new ForControlContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_forControl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			match(T__2);
			setState(228);
			variableAssigment();
			setState(229);
			match(END_STATEMENT);
			setState(230);
			expression(0);
			setState(231);
			match(END_STATEMENT);
			setState(232);
			variableAssigment();
			setState(233);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchCasesContext extends ParserRuleContext {
		public PossibleValuesContext possibleValues() {
			return getRuleContext(PossibleValuesContext.class,0);
		}
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public SwitchCasesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchCases; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterSwitchCases(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitSwitchCases(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitSwitchCases(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SwitchCasesContext switchCases() throws RecognitionException {
		SwitchCasesContext _localctx = new SwitchCasesContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_switchCases);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			match(T__14);
			setState(236);
			possibleValues();
			setState(237);
			match(T__15);
			setState(238);
			match(T__4);
			setState(239);
			blockCommand();
			setState(240);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchDefaultCaseContext extends ParserRuleContext {
		public BlockCommandContext blockCommand() {
			return getRuleContext(BlockCommandContext.class,0);
		}
		public SwitchDefaultCaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchDefaultCase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterSwitchDefaultCase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitSwitchDefaultCase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitSwitchDefaultCase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SwitchDefaultCaseContext switchDefaultCase() throws RecognitionException {
		SwitchDefaultCaseContext _localctx = new SwitchDefaultCaseContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_switchDefaultCase);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			match(T__16);
			setState(243);
			match(T__15);
			setState(244);
			match(T__4);
			setState(245);
			blockCommand();
			setState(246);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolContext extends ParserRuleContext {
		public PossibleTypesContext possibleTypes() {
			return getRuleContext(PossibleTypesContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(GramParser.IDENT, 0); }
		public SymbolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterSymbol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitSymbol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitSymbol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SymbolContext symbol() throws RecognitionException {
		SymbolContext _localctx = new SymbolContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_symbol);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			possibleTypes();
			setState(249);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolDeclAssignContext extends ParserRuleContext {
		public SymbolContext symbol() {
			return getRuleContext(SymbolContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(GramParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SymbolDeclAssignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbolDeclAssign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterSymbolDeclAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitSymbolDeclAssign(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitSymbolDeclAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SymbolDeclAssignContext symbolDeclAssign() throws RecognitionException {
		SymbolDeclAssignContext _localctx = new SymbolDeclAssignContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_symbolDeclAssign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(251);
			symbol();
			setState(252);
			match(ASSIGN);
			setState(253);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcParameterContext extends ParserRuleContext {
		public SymbolContext symbol() {
			return getRuleContext(SymbolContext.class,0);
		}
		public TerminalNode ADDRESS_KEYWORD() { return getToken(GramParser.ADDRESS_KEYWORD, 0); }
		public ProcParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).enterProcParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramListener) ((GramListener)listener).exitProcParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramVisitor) return ((GramVisitor<? extends T>)visitor).visitProcParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProcParameterContext procParameter() throws RecognitionException {
		ProcParameterContext _localctx = new ProcParameterContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_procParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			symbol();
			setState(257);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ADDRESS_KEYWORD) {
				{
				setState(256);
				match(ADDRESS_KEYWORD);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 7);
		case 2:
			return precpred(_ctx, 6);
		case 3:
			return precpred(_ctx, 5);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3&\u0106\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\3\2\3\2\3\2\3\3\5\3+\n\3\3\3\5\3.\n\3\3\3\7\3\61\n\3\f\3\16"+
		"\3\64\13\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4<\n\4\f\4\16\4?\13\4\3\4\3\4\3\5"+
		"\3\5\3\5\3\5\7\5G\n\5\f\5\16\5J\13\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\7"+
		"\6T\n\6\f\6\16\6W\13\6\5\6Y\n\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7l\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\6\7\u0094\n\7\r\7"+
		"\16\7\u0095\3\7\5\7\u0099\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00a3"+
		"\n\7\3\b\7\b\u00a6\n\b\f\b\16\b\u00a9\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00b8\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\7\t\u00c6\n\t\f\t\16\t\u00c9\13\t\3\n\3\n\3\n\3\n\3\n"+
		"\7\n\u00d0\n\n\f\n\16\n\u00d3\13\n\5\n\u00d5\n\n\3\n\3\n\3\13\3\13\3\f"+
		"\5\f\u00dc\n\f\3\f\3\f\5\f\u00e0\n\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3"+
		"\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3\23\5\23\u0104"+
		"\n\23\3\23\2\3\20\24\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$\2\3\3"+
		"\2\30\31\2\u0113\2&\3\2\2\2\4*\3\2\2\2\6\67\3\2\2\2\bB\3\2\2\2\nM\3\2"+
		"\2\2\f\u00a2\3\2\2\2\16\u00a7\3\2\2\2\20\u00b7\3\2\2\2\22\u00ca\3\2\2"+
		"\2\24\u00d8\3\2\2\2\26\u00df\3\2\2\2\30\u00e1\3\2\2\2\32\u00e5\3\2\2\2"+
		"\34\u00ed\3\2\2\2\36\u00f4\3\2\2\2 \u00fa\3\2\2\2\"\u00fd\3\2\2\2$\u0101"+
		"\3\2\2\2&\'\5\4\3\2\'(\7\3\2\2(\3\3\2\2\2)+\5\6\4\2*)\3\2\2\2*+\3\2\2"+
		"\2+-\3\2\2\2,.\5\b\5\2-,\3\2\2\2-.\3\2\2\2.\62\3\2\2\2/\61\5\n\6\2\60"+
		"/\3\2\2\2\61\64\3\2\2\2\62\60\3\2\2\2\62\63\3\2\2\2\63\65\3\2\2\2\64\62"+
		"\3\2\2\2\65\66\5\16\b\2\66\5\3\2\2\2\678\7\24\2\28=\5\"\22\29:\7\4\2\2"+
		":<\5\"\22\2;9\3\2\2\2<?\3\2\2\2=;\3\2\2\2=>\3\2\2\2>@\3\2\2\2?=\3\2\2"+
		"\2@A\7\35\2\2A\7\3\2\2\2BC\7\25\2\2CH\5 \21\2DE\7\4\2\2EG\5 \21\2FD\3"+
		"\2\2\2GJ\3\2\2\2HF\3\2\2\2HI\3\2\2\2IK\3\2\2\2JH\3\2\2\2KL\7\35\2\2L\t"+
		"\3\2\2\2MN\7\26\2\2NO\7#\2\2OX\7\5\2\2PU\5$\23\2QR\7\4\2\2RT\5$\23\2S"+
		"Q\3\2\2\2TW\3\2\2\2US\3\2\2\2UV\3\2\2\2VY\3\2\2\2WU\3\2\2\2XP\3\2\2\2"+
		"XY\3\2\2\2YZ\3\2\2\2Z[\7\6\2\2[\\\7\7\2\2\\]\5\4\3\2]^\7\b\2\2^\13\3\2"+
		"\2\2_`\7\t\2\2`a\7\5\2\2ab\5\20\t\2bc\7\6\2\2cd\7\7\2\2de\5\16\b\2ek\7"+
		"\b\2\2fg\7\n\2\2gh\7\7\2\2hi\5\16\b\2ij\7\b\2\2jl\3\2\2\2kf\3\2\2\2kl"+
		"\3\2\2\2l\u00a3\3\2\2\2mn\7\13\2\2no\5\32\16\2op\7\7\2\2pq\5\16\b\2qr"+
		"\7\b\2\2r\u00a3\3\2\2\2st\7\f\2\2tu\7\5\2\2uv\5\20\t\2vw\7\6\2\2wx\7\7"+
		"\2\2xy\5\16\b\2yz\7\b\2\2z\u00a3\3\2\2\2{|\7\r\2\2|}\7\7\2\2}~\5\16\b"+
		"\2~\177\7\b\2\2\177\u0080\7\f\2\2\u0080\u0081\7\5\2\2\u0081\u0082\5\20"+
		"\t\2\u0082\u0083\7\6\2\2\u0083\u00a3\3\2\2\2\u0084\u0085\7\16\2\2\u0085"+
		"\u0086\7\7\2\2\u0086\u0087\5\16\b\2\u0087\u0088\7\b\2\2\u0088\u0089\7"+
		"\17\2\2\u0089\u008a\7\5\2\2\u008a\u008b\5\20\t\2\u008b\u008c\7\6\2\2\u008c"+
		"\u00a3\3\2\2\2\u008d\u008e\7\20\2\2\u008e\u008f\7\5\2\2\u008f\u0090\5"+
		"\20\t\2\u0090\u0091\7\6\2\2\u0091\u0093\7\7\2\2\u0092\u0094\5\34\17\2"+
		"\u0093\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0093\3\2\2\2\u0095\u0096"+
		"\3\2\2\2\u0096\u0098\3\2\2\2\u0097\u0099\5\36\20\2\u0098\u0097\3\2\2\2"+
		"\u0098\u0099\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009b\7\b\2\2\u009b\u00a3"+
		"\3\2\2\2\u009c\u009d\5\22\n\2\u009d\u009e\7\35\2\2\u009e\u00a3\3\2\2\2"+
		"\u009f\u00a0\5\30\r\2\u00a0\u00a1\7\35\2\2\u00a1\u00a3\3\2\2\2\u00a2_"+
		"\3\2\2\2\u00a2m\3\2\2\2\u00a2s\3\2\2\2\u00a2{\3\2\2\2\u00a2\u0084\3\2"+
		"\2\2\u00a2\u008d\3\2\2\2\u00a2\u009c\3\2\2\2\u00a2\u009f\3\2\2\2\u00a3"+
		"\r\3\2\2\2\u00a4\u00a6\5\f\7\2\u00a5\u00a4\3\2\2\2\u00a6\u00a9\3\2\2\2"+
		"\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\17\3\2\2\2\u00a9\u00a7"+
		"\3\2\2\2\u00aa\u00ab\b\t\1\2\u00ab\u00b8\5\26\f\2\u00ac\u00b8\7#\2\2\u00ad"+
		"\u00ae\7\5\2\2\u00ae\u00af\5\20\t\2\u00af\u00b0\7\6\2\2\u00b0\u00b8\3"+
		"\2\2\2\u00b1\u00b2\7!\2\2\u00b2\u00b8\5\20\t\5\u00b3\u00b4\7\36\2\2\u00b4"+
		"\u00b8\5\20\t\4\u00b5\u00b6\7#\2\2\u00b6\u00b8\7\27\2\2\u00b7\u00aa\3"+
		"\2\2\2\u00b7\u00ac\3\2\2\2\u00b7\u00ad\3\2\2\2\u00b7\u00b1\3\2\2\2\u00b7"+
		"\u00b3\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b8\u00c7\3\2\2\2\u00b9\u00ba\f\n"+
		"\2\2\u00ba\u00bb\7\37\2\2\u00bb\u00c6\5\20\t\13\u00bc\u00bd\f\t\2\2\u00bd"+
		"\u00be\7\36\2\2\u00be\u00c6\5\20\t\n\u00bf\u00c0\f\b\2\2\u00c0\u00c1\7"+
		"\"\2\2\u00c1\u00c6\5\20\t\t\u00c2\u00c3\f\7\2\2\u00c3\u00c4\7 \2\2\u00c4"+
		"\u00c6\5\20\t\b\u00c5\u00b9\3\2\2\2\u00c5\u00bc\3\2\2\2\u00c5\u00bf\3"+
		"\2\2\2\u00c5\u00c2\3\2\2\2\u00c6\u00c9\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7"+
		"\u00c8\3\2\2\2\u00c8\21\3\2\2\2\u00c9\u00c7\3\2\2\2\u00ca\u00cb\7#\2\2"+
		"\u00cb\u00d4\7\5\2\2\u00cc\u00d1\5\20\t\2\u00cd\u00ce\7\4\2\2\u00ce\u00d0"+
		"\5\20\t\2\u00cf\u00cd\3\2\2\2\u00d0\u00d3\3\2\2\2\u00d1\u00cf\3\2\2\2"+
		"\u00d1\u00d2\3\2\2\2\u00d2\u00d5\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d4\u00cc"+
		"\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\7\6\2\2\u00d7"+
		"\23\3\2\2\2\u00d8\u00d9\t\2\2\2\u00d9\25\3\2\2\2\u00da\u00dc\7\36\2\2"+
		"\u00db\u00da\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00e0"+
		"\7$\2\2\u00de\u00e0\7\33\2\2\u00df\u00db\3\2\2\2\u00df\u00de\3\2\2\2\u00e0"+
		"\27\3\2\2\2\u00e1\u00e2\7#\2\2\u00e2\u00e3\7\34\2\2\u00e3\u00e4\5\20\t"+
		"\2\u00e4\31\3\2\2\2\u00e5\u00e6\7\5\2\2\u00e6\u00e7\5\30\r\2\u00e7\u00e8"+
		"\7\35\2\2\u00e8\u00e9\5\20\t\2\u00e9\u00ea\7\35\2\2\u00ea\u00eb\5\30\r"+
		"\2\u00eb\u00ec\7\6\2\2\u00ec\33\3\2\2\2\u00ed\u00ee\7\21\2\2\u00ee\u00ef"+
		"\5\26\f\2\u00ef\u00f0\7\22\2\2\u00f0\u00f1\7\7\2\2\u00f1\u00f2\5\16\b"+
		"\2\u00f2\u00f3\7\b\2\2\u00f3\35\3\2\2\2\u00f4\u00f5\7\23\2\2\u00f5\u00f6"+
		"\7\22\2\2\u00f6\u00f7\7\7\2\2\u00f7\u00f8\5\16\b\2\u00f8\u00f9\7\b\2\2"+
		"\u00f9\37\3\2\2\2\u00fa\u00fb\5\24\13\2\u00fb\u00fc\7#\2\2\u00fc!\3\2"+
		"\2\2\u00fd\u00fe\5 \21\2\u00fe\u00ff\7\34\2\2\u00ff\u0100\5\20\t\2\u0100"+
		"#\3\2\2\2\u0101\u0103\5 \21\2\u0102\u0104\7\27\2\2\u0103\u0102\3\2\2\2"+
		"\u0103\u0104\3\2\2\2\u0104%\3\2\2\2\26*-\62=HUXk\u0095\u0098\u00a2\u00a7"+
		"\u00b7\u00c5\u00c7\u00d1\u00d4\u00db\u00df\u0103";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}