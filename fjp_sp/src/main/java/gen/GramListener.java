package gen;// Generated from C:/Users/dandu/Documents/skola/FJP/fjp/fjp_sp/src\Gram.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GramParser}.
 */
public interface GramListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GramParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(GramParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(GramParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(GramParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(GramParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#blockConst}.
	 * @param ctx the parse tree
	 */
	void enterBlockConst(GramParser.BlockConstContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#blockConst}.
	 * @param ctx the parse tree
	 */
	void exitBlockConst(GramParser.BlockConstContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#blockVar}.
	 * @param ctx the parse tree
	 */
	void enterBlockVar(GramParser.BlockVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#blockVar}.
	 * @param ctx the parse tree
	 */
	void exitBlockVar(GramParser.BlockVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#blockProc}.
	 * @param ctx the parse tree
	 */
	void enterBlockProc(GramParser.BlockProcContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#blockProc}.
	 * @param ctx the parse tree
	 */
	void exitBlockProc(GramParser.BlockProcContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterIfCommand(GramParser.IfCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitIfCommand(GramParser.IfCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterForCommand(GramParser.ForCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitForCommand(GramParser.ForCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterWhileCommand(GramParser.WhileCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitWhileCommand(GramParser.WhileCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doWhileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileCommand(GramParser.DoWhileCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doWhileCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileCommand(GramParser.DoWhileCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repeatUntilCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterRepeatUntilCommand(GramParser.RepeatUntilCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repeatUntilCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitRepeatUntilCommand(GramParser.RepeatUntilCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code switchCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterSwitchCommand(GramParser.SwitchCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code switchCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitSwitchCommand(GramParser.SwitchCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code procedureCallCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterProcedureCallCommand(GramParser.ProcedureCallCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code procedureCallCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitProcedureCallCommand(GramParser.ProcedureCallCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableAssigmentCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssigmentCommand(GramParser.VariableAssigmentCommandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableAssigmentCommand}
	 * labeled alternative in {@link GramParser#command}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssigmentCommand(GramParser.VariableAssigmentCommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#blockCommand}.
	 * @param ctx the parse tree
	 */
	void enterBlockCommand(GramParser.BlockCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#blockCommand}.
	 * @param ctx the parse tree
	 */
	void exitBlockCommand(GramParser.BlockCommandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNegationExpression(GramParser.NegationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code negationExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNegationExpression(GramParser.NegationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valueExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterValueExpression(GramParser.ValueExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valueExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitValueExpression(GramParser.ValueExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plusMinusExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlusMinusExpression(GramParser.PlusMinusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plusMinusExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlusMinusExpression(GramParser.PlusMinusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plusMinusBeforeExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlusMinusBeforeExpression(GramParser.PlusMinusBeforeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plusMinusBeforeExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlusMinusBeforeExpression(GramParser.PlusMinusBeforeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCompExpression(GramParser.CompExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCompExpression(GramParser.CompExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andOrExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndOrExpression(GramParser.AndOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andOrExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndOrExpression(GramParser.AndOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multDivExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultDivExpression(GramParser.MultDivExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multDivExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultDivExpression(GramParser.MultDivExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenExpression(GramParser.ParenExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenExpression(GramParser.ParenExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identAddressExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentAddressExpression(GramParser.IdentAddressExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identAddressExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentAddressExpression(GramParser.IdentAddressExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentExpression(GramParser.IdentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identExpression}
	 * labeled alternative in {@link GramParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentExpression(GramParser.IdentExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#procedureCall}.
	 * @param ctx the parse tree
	 */
	void enterProcedureCall(GramParser.ProcedureCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#procedureCall}.
	 * @param ctx the parse tree
	 */
	void exitProcedureCall(GramParser.ProcedureCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#possibleTypes}.
	 * @param ctx the parse tree
	 */
	void enterPossibleTypes(GramParser.PossibleTypesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#possibleTypes}.
	 * @param ctx the parse tree
	 */
	void exitPossibleTypes(GramParser.PossibleTypesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#possibleValues}.
	 * @param ctx the parse tree
	 */
	void enterPossibleValues(GramParser.PossibleValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#possibleValues}.
	 * @param ctx the parse tree
	 */
	void exitPossibleValues(GramParser.PossibleValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#variableAssigment}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssigment(GramParser.VariableAssigmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#variableAssigment}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssigment(GramParser.VariableAssigmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#forControl}.
	 * @param ctx the parse tree
	 */
	void enterForControl(GramParser.ForControlContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#forControl}.
	 * @param ctx the parse tree
	 */
	void exitForControl(GramParser.ForControlContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#switchCases}.
	 * @param ctx the parse tree
	 */
	void enterSwitchCases(GramParser.SwitchCasesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#switchCases}.
	 * @param ctx the parse tree
	 */
	void exitSwitchCases(GramParser.SwitchCasesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#switchDefaultCase}.
	 * @param ctx the parse tree
	 */
	void enterSwitchDefaultCase(GramParser.SwitchDefaultCaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#switchDefaultCase}.
	 * @param ctx the parse tree
	 */
	void exitSwitchDefaultCase(GramParser.SwitchDefaultCaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#symbol}.
	 * @param ctx the parse tree
	 */
	void enterSymbol(GramParser.SymbolContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#symbol}.
	 * @param ctx the parse tree
	 */
	void exitSymbol(GramParser.SymbolContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#symbolDeclAssign}.
	 * @param ctx the parse tree
	 */
	void enterSymbolDeclAssign(GramParser.SymbolDeclAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#symbolDeclAssign}.
	 * @param ctx the parse tree
	 */
	void exitSymbolDeclAssign(GramParser.SymbolDeclAssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramParser#procParameter}.
	 * @param ctx the parse tree
	 */
	void enterProcParameter(GramParser.ProcParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramParser#procParameter}.
	 * @param ctx the parse tree
	 */
	void exitProcParameter(GramParser.ProcParameterContext ctx);
}