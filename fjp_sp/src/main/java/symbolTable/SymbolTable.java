package symbolTable;

import java.util.ArrayList;
import java.util.HashMap;


public class SymbolTable {

    /**
     * each HashMap represents new level, the lowest level is at index 0
     * key is symbol name
     */
    ArrayList< HashMap<String, SymbolTableItem> > table = new ArrayList<>();

    public void addSymbol(int level, String key, SymbolTableItem value){
        table.get(level).put(key, value);
    }

    public void addSymbolAtTopLevel(String key, SymbolTableItem value){
        addSymbol(table.size() - 1, key, value);
    }

    public void addLevel(){
        table.add(new HashMap<>());
    }

    public void removeLevel(){
        table.remove(table.size()-1);
    }

    /**
     * Search for symbol starting from the uppermost level
     * @param key symbol name
     * @return SymbolTableItem or null if not found
     */
    public SymbolTableItem getSymbol(String key){
        for(int i = table.size() -1; i >= 0; i--){
            SymbolTableItem item = table.get(i).get(key);
            if(item != null){
                return  item;
            }
        }
        return null;
    }

    /**
     * search for symbol at top level only
     * @param key symbol name
     * @return SymbolTableItem or null if not found
     */
    public SymbolTableItem getSymbolFromTopLevel(String key){
        return table.get(table.size() -1).get(key);
    }

    /**
     * returns true if symbol is already defined at the uppermost level
     * @param key name
     */
    public boolean existsSymbolAtTopLevel(String key){
        return table.get(table.size() -1).get(key) != null;
    }

}
