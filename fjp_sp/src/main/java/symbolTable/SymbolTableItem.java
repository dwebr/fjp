package symbolTable;

import types.Symbol;
import types.enums.SymbolType;

import java.util.List;

/**
 * item od the symbol table
 */
public class SymbolTableItem {

    private final String name;
    private final int address;

    // procedure / value / constant
    private final SymbolTableItemType type;

    // int / bool / int address / boolean address
    private SymbolType dataType;

    private List<Symbol> procParameters;

    public SymbolTableItem(String name, int address, SymbolTableItemType type, SymbolType dataType) {
        this.name = name;
        this.address = address;
        this.type = type;
        this.dataType = dataType;
    }


    public SymbolTableItem(String name, int address, SymbolTableItemType type, List<Symbol> procParameters) {
        this.name = name;
        this.address = address;
        this.type = type;
        this.procParameters = procParameters;
    }

    public SymbolType getDataType() {
        return dataType;
    }

    public List<Symbol> getProcParameters() {
        return procParameters;
    }

    public int getAddress() {
        return address;
    }

    public SymbolTableItemType getType() {
        return type;
    }
    public boolean isAddress(){
        return dataType == SymbolType.INT_ADDR || dataType == SymbolType.BOOLEAN_ADDR;
    }
}
