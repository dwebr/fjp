package symbolTable;

public enum SymbolTableItemType {
    PROCEDURE,
    VARIABLE,
    CONSTANT
}
